Updated in September and December 2012 by Ivan Zakharyaschev imz at altlinux.org

* unify with a FISH client

My idea here is similar to that of [FISH](http://en.wikipedia.org/wiki/Files_transferred_over_shell_protocol) -- <http://unix.stackexchange.com/a/3959/4319>, <http://unix.stackexchange.com/a/8735/4319> ("FISH = ssh + dd"): no server required, only basic Unix "ls", and "cat", and etc.

Also, [tramp-fish.el](http://lists.gnu.org/archive/html/tramp-devel/2010-09/msg00001.html) could be useful for transferring files to Android, so it shouldn't have been thrown away from Emacs TRAMP.

* re-write it into a single file

Less repetitions in the code, common processing of args and common options.

More combinations of options should be available then.

This will shape the code in a more aspect-oriented way.

** (Perhaps, try to re-write it in a nicer lanuage like Lisp, Haskell/Curry, attribute-grammars... 

because I again had the possibilty to hate bash's unpredictable ugliness and incompleteness.)

** The names for the commands could be "rpush cat", "rpush continue", "rpush into-dir".

* +1 feature: retries (like "wget") that can continue interrupted transfer -- for all the commands.

* "rput-into" should also accept the "ls"-format-choosing switches (Android vs Unix)

* Do we want one more command, like "rpush-dir" for copying whole directories?
Or this is not in the line of Unix philosophy of small utuilities that can be combined?
But here, we'd loose something if it's a combination: we can't reuse the same ssh session.

The combination concept of Haskell monads or the like would be more useful here.
